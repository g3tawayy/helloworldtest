public class Hello
{
    private int language;

    // TODO
    // Ajouter autres langues
    // Anglais  : Hello
	// Chinois  : Nihow
    // Espagnol : Hola
    // Russe    : Привет
    private static String[] greeting = { "Salut" };

    // TODO
    // Ajouter autres langues
    // Anglais  : 1
	// Chinois  : 2
    // Espagnol : 3
    // Russe    : 4
    private static String[] languages = { "0 - Français" };

    public Hello(int language) {
        // TODO
        // Ajouter vérification d'index
        //     Si index invalide : retourner greeting Français
        this.language = language;
    }

    public String GetGreeting() {
        return greeting[this.language];
    }

    public static String[] GetAvailableLanguages() {
        return languages;
    }
}
